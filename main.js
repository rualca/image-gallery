const currentImage =  document.querySelector('#current');
const imgs = document.querySelectorAll('.imgs img');
const opacity = 0.4;

function resetOpacityImages() {
  imgs.forEach((image) => {
    image.style.opacity = 1;
  })
}

function imgClick(event) {
  resetOpacityImages();
  currentImage.src = event.target.src;
  currentImage.classList.add('fade-in');

  setTimeout(() => {
    currentImage.classList.remove('fade-in');
  }, 500);

  event.target.style.opacity = opacity; 
}

imgs[0].style.opacity = opacity;

imgs.forEach((image) => {
  image.addEventListener('click', imgClick);
});